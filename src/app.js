const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const flash = require('connect-flash')
const session = require('express-session')

const morgan = require('morgan')
const app = express()
const router = require('./router/route.js')
//app.use(express.static(path.join(__dirname,'views')))


app.set('views',path.join(__dirname,'views'))
app.set('view engine', 'hbs')
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
//app.use(express.json())
app.use(express.static(path.join(__dirname,'public')))
app.use(session({ 
									cookie: { maxAge: 60000 }, 
                  secret: 'woot',
                  resave: false, 
                  saveUninitialized: false
                }))
app.use(flash())
app.use((req, res, next) => {
	app.locals.message = req.flash('message')
	app.locals.usuario = req.flash('usuario')
	app.locals.password = req.flash('password')
	next()
})
app.use(router)
app.set('port',process.env.PORT || 3001)
app.listen(app.get('port'),()=>{
	console.log('iniciado servidor:',app.get('port'))
})