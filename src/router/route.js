/** 
 * CONFIG ROUTER 
**/
const express = require('express')
const router = express.Router()
const pool = require('../connect')

	router.get('/',(req, res, next) => {
		res.render('index')
	})
	router.post('/auth',async (req, res, next) => {
		const {usuario , password} = req.body
		if(usuario==''){
			req.flash('message','el campo usuario es obligatorio.')
			req.flash('password',password)
			res.redirect('back')
		}
		if(password==''){
			req.flash('message','el campo contraseña es obligatorio.')
			req.flash('usuario',usuario)
			res.redirect('back')
		}
		const rows = await pool.query("SELECT * FROM usuarios WHERE usuario = $1 AND password = $2",[usuario,password])
		if(rows.rowCount > 0){
			res.render('site')

		}else{
			res.render('site')
			req.flash('message','el usuario no se encuentra registrado.')
			res.redirect('back')
			//next()
		}
	})

module.exports = router